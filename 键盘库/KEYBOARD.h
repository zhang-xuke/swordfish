#ifndef KEYBOARD_H_INCLUDED
#define KEYBOARD_H_INCLUDED
#include"Arduino.h"
    const int numRows=4;//行
    const int numCols=4;//列
    const int debounceTime=20;
    const char keymap_1[numRows][numCols]=
    {
        {'1','2','3','+'},
        {'4','5','6','-'},
        {'7','8','9','=' },
        {'*','0','#','/'}
    };
    const char keymap_2[numRows][numCols]=
    {
        {'2','3','4','+'},
        {'5','5','2','-'},
        {'7','8','9','=' },
        {'*','0','#','/'}
    };
    const int rowPins[numRows]={2,3,4,5};//输入四个行管脚
    const int colPins[numCols]={6,7,8,9};//输入四个列管脚
    void initKeyboard();
    char input();
    char getkey();




#endif // KEYBOARD_H_INCLUDED
