#include"Keyboard.h"
int flag = 1;
    void initKeyboard()
{
     flag=1;
     for(int row=0;row<numRows;row++){
         pinMode(rowPins[row],INPUT);
         digitalWrite(rowPins[row],HIGH);
     }
     for(int column=0;column< numCols;column++){
         pinMode(colPins[column],OUTPUT);
         digitalWrite(colPins[column],HIGH);
     }
}
char input(){
     char key=0;
     while(key==0)key=getkey();
     return key;
}

char getkey(){
     char key=0;
     for(int column=0;column<numCols;column++){
        digitalWrite(colPins[column],LOW);
        for(int row=0;row<numRows;row++){
            if(digitalRead(rowPins[row])==LOW){
               delay(debounceTime);
               while(digitalRead(rowPins[row])==LOW);
               if(flag==1)key=keymap_1[row][column];
               else       key=keymap_2[row][column];
                if(key=='#'){
                    if(flag==2)flag=1;
                    else       flag=2;

                }

            }
        }
        digitalWrite(colPins[column],HIGH);//De-active the current column
     }
     if(key=='#') return 0;
     else return key;
}


