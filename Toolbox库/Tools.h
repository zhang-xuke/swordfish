#ifndef TOOLS_H_INCLUDED
#define TOOLS_H_INCLUDED
#include"Arduino.h"
#include<stdlib.h>

using namespace std;

# define Precision 0.0001//积分精度要求
# define e         2.71828183
constexpr auto MAXRepeat = 100; //最大允许重复



template<class elemType>
class SeqStack
{
private:
    elemType* elem;
    int top_p;
    int maxSize;
    void doubleSpace();
public:
    SeqStack(int initSize = 10);
    ~SeqStack();
    bool isEmpty()const;
    void push(const elemType& x);
    elemType pop();
    elemType top()const;
};
int pri(char a);
struct Node {
    double num;//操作数
    char op;//操作符
    char spe;//未知数x,y
    int type;//1--操作数；2--操作符；3--x;4--特殊函数(sin,cos,ln)
};
template <class elemType>
class Queue
{
private:
    elemType* elem;
    int front, rear;
    int MaxSize;
    void doubleSpace()
    {
        elemType* tmp = elem;
        elem = new elemType[2 * MaxSize];
        for (int i = 1; i <= MaxSize; ++i)
            elem[i] = tmp[(front + i) % MaxSize];
        front = 0; rear = MaxSize;
        MaxSize *= 2;
        delete[] tmp;
    };
public:
    Queue(int size = 10)
    {
        elem = new elemType[size];
        front = rear = 0;
        MaxSize = size;
    }
    Queue(Queue<elemType>& x)
    {
        MaxSize = x.MaxSize;
        elem = new elemType[MaxSize];
        front = x.front;
        rear = x.rear;
        for (int i = 0; i < MaxSize; ++i)
        {
            elem[i] = x.elem[i];
        }
    }
    void clear()
    {
        front = rear = 0;
    }
    bool isEmpty()
    {
        return front == rear;
    }
    elemType deQueue()
    {
        front = (front + 1) % MaxSize;
        return elem[front];
    }
    elemType getHead()
    {
        return elem[(front + 1) % MaxSize];
    }
    void enQueue(const elemType& x)
    {
        if ((rear + 1) % MaxSize == front) doubleSpace();
        rear = (rear + 1) % MaxSize;
        elem[rear] = x;
    }
    elemType* visit(int sq)
    {
        return &elem[(front + sq) % MaxSize];
    }
    int getlength()
    {
        return (abs(rear - front));
    }
    ~Queue()
    {
        delete[] elem;
    }
};
struct calcuator
{
    Queue<Node> q;
    double average;
    double Variance;
    int error=0;
    void change(char* p, int length);
    double function(double x=0,double y=0);
    double Romberg(double a, double b);
    void Statistic(int n, double* data);
};
class calc
{
public:

    char* expression;//要计算的中缀表达式

    enum token { OPAREN, ADD, SUB, MULTI, DIV, EXP, CPAREN, VALUE, EOL };

    void BinaryOp(token op, SeqStack<double>& dataStack);
    token getOp(double& value);


    calc(char* s) { expression = new char[strlen(s) + 1]; strcpy(expression, s); }
    ~calc() { delete expression; }
    double result();//计算表达式结果
};




#endif // KEYBOARD_H_INCLUD

