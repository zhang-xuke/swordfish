#include"Tools.h"



int pri(char a)
{
    if (a == '+' || a == '-') return 1;
    else if (a == '*' || a == '/') return 2;
    else if (a == '^') return 3;
    else if (a == 'd' || a == 'Q' || a == 'L') return 4;
    else return 0;
}


template<class elemType>
SeqStack<elemType>::SeqStack(int initSize)
{
    elem = new elemType[initSize];
    maxSize = initSize;
    top_p = -1;

}


template<class elemType>
SeqStack<elemType>::~SeqStack()
{
    delete[]elem;
}


template<class elemType>
bool SeqStack<elemType>::isEmpty()const
{
    return top_p == -1;
}


template<class elemType>
void SeqStack<elemType>::push(const elemType& x)
{
    if (top_p + 1 == maxSize)  doubleSpace();//如果栈满就扩大栈的容量


    elem[++top_p] = x;         //元素入栈

}


template<class elemType>
elemType SeqStack<elemType>::pop()
{
    return elem[top_p--];
}



template<class elemType>
elemType SeqStack<elemType>::top() const
{
    return elem[top_p];
}
template<class elemType>
void SeqStack<elemType>::doubleSpace()
{
    elemType* tmp = elem;
    elem = new elemType[2 * maxSize];         //申请新空间


    for (int i = 0; i <= maxSize - 1; ++i)           elem[i] = tmp[i];


    maxSize *= 2;          //最大空间变成2倍
    delete[]tmp;         //释放旧空间
}

void calcuator::change(char* p, int length)
{
    SeqStack<Node> s;
    Node temp;
    q.clear();
    int judge = 0;
    for (int i = 0; i < length;)
    {
        if (p[i] == '(') {//3.遇到左括号：将其入栈
            temp.type==2;
            temp.op = p[i];
            s.push(temp);
            i++;
        }
        else if (p[i] == ')'){//4.遇到右括号：执行出栈操作，输出到后缀表达式，直到弹出的是左括号
            while (!s.isEmpty() && s.top().op != '('){
                q.enQueue(s.pop());
            }
            s.pop();//弹出左括号
            i++;
        }
        else if ((p[i] >= '0'&&p[i] <= '9')||p[i]=='e'||p[i]=='T'){			//如果是数字
            temp.type = 1;
            if (p[i] >= '0' && p[i] <= '9')
                temp.num = p[i] - '0';
            else if (p[i] == 'e')
                temp.num = e;
            else
                temp.num = PI;
            i++;//后移一位,因为数字不一定是个位数
            while (i < length && ((p[i] >= '0'&&p[i] <= '9')||p[i]=='.')){
                if (p[i] == '.') judge = 1;
                else {
                    if (judge == 0)
                        temp.num = temp.num * 10 + (p[i] - '0');
                    else {
                        temp.num = temp.num + (p[i] - '0') / pow(10,judge);
                        judge++;
                    }
                }
                i++;
            }
            judge = 0;
            q.enQueue(temp);//操作数进入后缀表达式
        }
        else if (p[i] == 'x')
        {
            temp.type = 3;
            temp.spe = 'x';
            q.enQueue(temp);
            ++i;
        }
        else if (p[i] == 'y')
        {
            temp.type = 3;
            temp.spe = 'y';
            q.enQueue(temp);
            ++i;
        }
        else{
            if (p[i] == '+' || p[i] == '-' || p[i] == '*' || p[i] == '/' || p[i] == '^')//如果是操作符			//5.遇到其他运算符：弹出所有优先加大于或等于该运算符的栈顶元素，然后将该运算符入栈
                temp.type = 2;
            else if (p[i]=='d'||p[i]=='L'||p[i]=='Q')
                temp.type = 4;
            while (!s.isEmpty() && pri(s.top().op)>=pri(p[i])){
                q.enQueue(s.pop());
            }
            temp.op = p[i];
            s.push(temp);
            i++;
        }
    }	//6.将栈中剩余内容依次弹出后缀表达式
    while (!s.isEmpty())
    q.enQueue(s.pop());
}
double calcuator::function(double x,double y)
{
    Node temp;
    double fu1, fu2,fu;
    SeqStack<double> qs;
    Queue<Node> tm(q);
    while (!tm.isEmpty())
    {
        temp = tm.deQueue();
        if (temp.type == 1)
            qs.push(temp.num);
        else if (temp.type == 3)
        {
            if (temp.spe == 'x')
                qs.push(x);
            else if (temp.spe == 'y')
                qs.push(y);
        }
        else if (temp.type == 2)
        {
            if (qs.isEmpty())
            {
                error = 3;
                break;
            }
            else {
                if (temp.op == '+')
                {
                    fu1 = qs.pop();
                    fu2 = qs.pop();
                    qs.push(fu1 + fu2);
                }
                else if (temp.op == '-')
                {
                    fu1 = qs.pop();
                    fu2 = qs.pop();
                    qs.push(fu2 - fu1);
                }
                else if (temp.op == '*')
                {
                    fu1 = qs.pop();
                    fu2 = qs.pop();
                    qs.push(fu2 * fu1);
                }
                else if (temp.op == '/')
                {
                    fu1 = qs.pop();
                    fu2 = qs.pop();
                    if (fu1 == 0) error = 1;
                    qs.push(fu2 / fu1);
                }
                else if (temp.op == '^')
                {
                    fu1 = qs.pop();
                    fu2 = qs.pop();
                    qs.push(pow(fu2, fu1));
                }
            }
        }
        else if (temp.type == 4)
        {
            if (qs.isEmpty())
                error = 3;
            if (temp.op == 'L')
            {
                fu1 = qs.pop();
                qs.push(sin(fu1));
                
            }
            else if (temp.op == 'Q')
            {
                fu1 = qs.pop();
                qs.push(cos(fu1));
            }
            else if (temp.op == 'd')
            {
                fu1 = qs.pop();
                if (fu1 <= 0) error = 2;
                qs.push(log(fu1));
            }

        }
    }
    return qs.top();
}
double calcuator::Romberg(double a, double b)
{
    int m, n, k;
    double y[MAXRepeat], h, ep, p, xk, s, q;
    h = b - a;
    y[0] = h * (function(a) + function(b)) / 2.0;//计算T`1`(h)=1/2(b-a)(f(a)+f(b));
    m = 1;
    n = 1;
    ep = Precision + 1;
    int num = 0;
    while ((ep >= Precision) && (m < MAXRepeat))
    {
        p = 0.0;
        for (k = 0; k < n; k++)
        {
            xk = a + (k + 0.5) * h; //   n-1
            p = p + function(xk);     //计算∑f(xk+h/2),T
        }                   //   k=0
        p = (y[0] + h * p) / 2.0;  //T`m`(h/2),变步长梯形求积公式
        s = 1.0;
        for (k = 1; k <= m; k++)
        {
            s = 4.0 * s;// pow(4,m)
            q = (s * p - y[k - 1]) / (s - 1.0);//[pow(4,m)T`m`(h/2)-T`m`(h)]/[pow(4,m)-1],2m阶牛顿柯斯特公式，即龙贝格公式
            y[k - 1] = p;
            p = q;
        }
        ep = fabs(q - y[m - 1]);//前后两步计算结果比较求精度
        m = m + 1;
        y[m - 1] = q;
        n = n + n;   //  2 4 8 16
        h = h / 2.0;//二倍分割区间
        num++;
    }
    return q;
}
    void calcuator::Statistic(int n, double* data)
    {
        average = 0;
        Variance = 0;
        for (int i = 0; i < n; ++i)
        {
            average += (data[i] / n);
        }
        for (int j = 0; j < n; ++j)
        {
            Variance += pow((data[j] - average), 2) / (n - 1);
        }
        Variance = pow(Variance, 0.5);
    }


void calc::BinaryOp(token op, SeqStack<double>& dataStack)
{
	double num1, num2;
	if (dataStack.isEmpty())
	{
		//lcd.print("error");
		return;
	}
	else num2 = dataStack.pop();
	if (dataStack.isEmpty())
	{
		//lcd.print("error");
		return;
	}
	else num1 = dataStack.pop();
	switch (op)
	{
	case ADD:dataStack.push(num1 + num2); break;
	case SUB:dataStack.push(num1 - num2); break;
	case MULTI:dataStack.push(num1 * num2); break;
	case DIV:dataStack.push(num1 / num2); break;
	case EXP:dataStack.push(pow(num1, num2));
	}

}

calc::token calc::getOp(double& value)
{
	int num = 0;
	while (*expression && *expression == ' ')
		++expression;

	if ((*expression) == '\0')  return EOL;

	value = 0;
	if (*expression <= '9' && *expression >= '0')
	{
		value = 0;
		while (*expression >= '0' && *expression <= '9')
		{
			value = value * 10 + *expression - '0';
			++expression;
		}

		if (*expression == '.') ++expression;

		while (*expression >= '0' && *expression <= '9')
		{
			value = value * 10 + *expression - '0';
			++expression;
			++num;
		}

		while (num != 0)
		{
			value = value / 10;
			--num;
		}
		return VALUE;
	}

	switch (*expression)
	{
	case'(':++expression; return OPAREN;
	case')':++expression; return CPAREN;
	case'+':++expression; return ADD;
	case'-':++expression; return SUB;
	case'*':++expression; return MULTI;
	case'/':++expression; return DIV;
	case'^':++expression; return EXP;
	}
}

double calc::result()
{
	token lastOp, topOp;
	double result_value, CurrentValue;
	SeqStack<token>opStack;
	SeqStack<double>dataStack;
	char* str = expression;

	while ((lastOp = getOp(CurrentValue)) != EOL)
		switch (lastOp)
		{
		case VALUE:dataStack.push(CurrentValue); break;
		case CPAREN:
			while (!opStack.isEmpty() && (topOp = opStack.pop()) != OPAREN)
				BinaryOp(topOp, dataStack);
			if (topOp != OPAREN)   Serial.print("error3"); break;
		case OPAREN:opStack.push(OPAREN); break;
		case EXP:opStack.push(EXP); break;
		case MULTI:case DIV:
			while (!opStack.isEmpty() && opStack.top() >= MULTI)
				BinaryOp(opStack.pop(), dataStack);
			opStack.push(lastOp);
			break;
		case ADD:case SUB:
			while (!opStack.isEmpty() && opStack.top() != OPAREN)
				BinaryOp(opStack.pop(), dataStack);
			opStack.push(lastOp);
		}

	while (!opStack.isEmpty())  BinaryOp(opStack.pop(), dataStack);
	if (dataStack.isEmpty())
	{
		Serial.print("error1");
		return 0;
	}
	result_value = dataStack.pop();
	if (!dataStack.isEmpty())
	{
        Serial.print("error1");
		return 0;
	}

	expression = str;
	return result_value;
}

